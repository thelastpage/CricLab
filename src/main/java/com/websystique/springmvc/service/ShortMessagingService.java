package com.websystique.springmvc.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedHashMap;

import org.springframework.stereotype.Service;

import com.plivo.helper.api.client.RestAPI;
import com.plivo.helper.api.response.message.MessageResponse;
import com.plivo.helper.exception.PlivoException;
import com.websystique.springmvc.model.Stats;

@Service
public class ShortMessagingService {
	
	public String createMessage(Stats st){
		String str[]=new String[4];
		str[0]=st.getScore().getTeam().get(0).getSideName();
		str[1]=st.getScore().getTeam().get(0).getInngs().getRun();
		str[2]=st.getScore().getTeam().get(0).getInngs().getWkts();
		str[3]=st.getScore().getTeam().get(0).getInngs().getOvers();
		
		String str2[]=new String[4];
		str2[0]=st.getScore().getTeam().get(1).getSideName();
		str2[1]=st.getScore().getTeam().get(1).getInngs().getRun();
		str2[2]=st.getScore().getTeam().get(1).getInngs().getWkts();
		str2[3]=st.getScore().getTeam().get(1).getInngs().getOvers();
		
		String msg="";
		msg+=(str[0]+"%0A"+"Runs=%20"+str[1]+" Wickets=%20"+str[2]+" Overs=%20"+str[3]+"%0A");
		msg+=(str2[0]+"%0A"+"Runs=%20"+str2[1]+" Wickets=%20"+str2[2]+" Overs=%20"+str2[3]);
		msg+="%0A"+st.getState().getStatus();
		//System.out.println(msg);
		return msg;
	}
	public void message(String msg) {
		
		try {
			String convert="https://control.msg91.com/api/sendhttp.php?authkey=yourAuthenticationKey&mobiles=mobileNumbersSeparatedByCommas&message="+msg+"&sender=ABCDEF&route=4&country=91";
			//URI uri= new URI(convert);
			URL url=new URL(convert);
			//URL url= new URL("https://control.msg91.com/api/longcodeBalance.php?authkey=156678AZaUd1J8URC759463cfc&type=4");
			HttpURLConnection conn=(HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			String line;
			StringBuilder builder = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			while((line = reader.readLine()) != null) {
			 builder.append(line);
			}
			System.out.println(builder);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}
}
