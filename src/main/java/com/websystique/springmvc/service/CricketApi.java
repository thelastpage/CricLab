package com.websystique.springmvc.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.stereotype.Service;

import com.websystique.springmvc.model.InningsDetails;
import com.websystique.springmvc.model.Score;
import com.websystique.springmvc.model.State;
import com.websystique.springmvc.model.Stats;
import com.websystique.springmvc.model.Team;
import com.websystique.springmvc.model.TeamInnings;

@Service
public class CricketApi {

	public Stats apiCall() {

		Stats st=new Stats();
		Score sc=new Score();
		Team bwlnTeam= new Team();

		try {
			URL url= new URL("http://synd.cricbuzz.com/j2me/1.0/livematches.xml");
			HttpURLConnection conn= (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			String output;
			String str="";
		
			while ((output = br.readLine()) != null) {
				str+=output;
			}
			JSONObject xmlJSONObj = XML.toJSONObject(str);
			String jsonPrettyPrintString = xmlJSONObj.toString(4);
			//System.out.println(jsonPrettyPrintString); 
			JSONObject obj= new JSONObject(jsonPrettyPrintString);
			JSONObject objMatch=(JSONObject) obj.opt("mchdata");
			int n= objMatch.optInt("NMchs");
			JSONArray arr= objMatch.getJSONArray("match");


			JSONObject matchDtls= arr.getJSONObject(0);
			st.setDataPath(matchDtls.getString("datapath"));

			if(matchDtls.has("mscr")){
				JSONObject matchScore=matchDtls.getJSONObject("mscr");
				JSONObject bwlng=matchScore.getJSONObject("blgTm");
				bwlnTeam.setSideName(bwlng.getString("sName"));
				List<Team> lsit= new ArrayList<Team>();
				JSONObject Inngs;
				if(bwlng.has("Inngs")){
					Inngs= bwlng.getJSONObject("Inngs");


					TeamInnings bwlnInngs=new TeamInnings();
					bwlnInngs.setDeclare(String.valueOf((Inngs.get("Decl"))));
					bwlnInngs.setDescription(Inngs.getString("desc"));
					bwlnInngs.setFollowOn(String.valueOf(Inngs.get("FollowOn")));
					bwlnInngs.setOvers(String.valueOf(Inngs.get("ovrs")));
					bwlnInngs.setRun(String.valueOf(Inngs.get("r")));
					bwlnInngs.setWkts(String.valueOf(Inngs.get("wkts")));
					bwlnTeam.setInngs(bwlnInngs);
					lsit.add(bwlnTeam);
				}

				if(bwlng.has("Inngs")){
					Team btTeam=new Team();
					bwlng=matchScore.getJSONObject("btTm");
					btTeam.setSideName(bwlng.getString("sName"));
					Inngs= bwlng.getJSONObject("Inngs");

					TeamInnings btnInngs=new TeamInnings();
					btnInngs.setDeclare(String.valueOf((Inngs.get("Decl"))));
					btnInngs.setDescription(Inngs.getString("desc"));
					btnInngs.setFollowOn(String.valueOf(Inngs.get("FollowOn")));
					btnInngs.setOvers(String.valueOf(Inngs.get("ovrs")));
					btnInngs.setRun(String.valueOf(Inngs.get("r")));
					btnInngs.setWkts(String.valueOf(Inngs.get("wkts")));
					btTeam.setInngs(btnInngs);
					lsit.add(btTeam);
				}
				sc.setTeam(lsit);
				JSONObject Inngsdtl=matchScore.getJSONObject("inngsdetail");

				InningsDetails innDtls=new InningsDetails();
				innDtls.setCurrentPartnership(String.valueOf(Inngsdtl.get("cprtshp")));
				innDtls.setCurrentRunRate(String.valueOf(Inngsdtl.get("crr")));
				innDtls.setNumberOfOvers(String.valueOf(Inngsdtl.get("noofovers")));
				innDtls.setRequiredRunRate(String.valueOf(Inngsdtl.get("rrr")));

				sc.setDetails(innDtls);
				st.setScore(sc);
			}
			st.setMatchNumber(String.valueOf(matchDtls.get("mnum")));
			st.setInningsCount(String.valueOf(matchDtls.get("inngCnt")));
			st.setMatchType(String.valueOf(matchDtls.get("type")));
			st.setVenueCity(String.valueOf(matchDtls.get("vcity")));
			st.setGround(String.valueOf(matchDtls.get("grnd")));
			st.setMatchDescription(String.valueOf(matchDtls.get("mchDesc")));
			st.setSeries(String.valueOf(matchDtls.get("srs")));

			State sta=new State();
			JSONObject state= matchDtls.getJSONObject("state");
			sta.setDecision(String.valueOf(state.get("decisn")));
			sta.setMatchState(String.valueOf(state.get("mchState")));
			sta.setToss(String.valueOf(state.get("TW")));
			sta.setStatus(String.valueOf(state.get("status")));

			st.setState(sta);





		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return st;

	}
}
