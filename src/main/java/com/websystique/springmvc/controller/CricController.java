package com.websystique.springmvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.websystique.springmvc.model.Stats;
import com.websystique.springmvc.service.CricketApi;
import com.websystique.springmvc.service.ShortMessagingService;
 
@RestController
public class CricController {
 
    
    @Autowired
    CricketApi cricAPI;
    
    @Autowired
    ShortMessagingService sms;
    //Service which will do all data retrieval/manipulation work
 
    
    //-------------------Retrieve the data--------------------------------------------------------
     
    @RequestMapping(value = "/cric/", method = RequestMethod.GET)
    public ResponseEntity<Stats> listAlldata() {
        Stats stats = cricAPI.apiCall();
        if(null==stats){
            return new ResponseEntity<Stats>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        String msg=sms.createMessage(stats);
        //sms.message(msg);  //This for sending the updates to your mobile via sms
        return new ResponseEntity<Stats>(stats, HttpStatus.OK);
    }
 
 
}