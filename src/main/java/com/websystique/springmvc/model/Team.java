package com.websystique.springmvc.model;

public class Team {
	String sideName;
	TeamInnings inngs;
	public String getSideName() {
		return sideName;
	}
	public void setSideName(String sideName) {
		this.sideName = sideName;
	}
	public TeamInnings getInngs() {
		return inngs;
	}
	public void setInngs(TeamInnings inngs) {
		this.inngs = inngs;
	}
	
	
}
