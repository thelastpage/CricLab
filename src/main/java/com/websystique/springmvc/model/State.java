package com.websystique.springmvc.model;

public class State {
	String decision;
	String toss;
	String specialStatus;
	String matchState;
	String addOnStatus;
	String status;
	public String getDecision() {
		return decision;
	}
	public void setDecision(String decision) {
		this.decision = decision;
	}
	public String getToss() {
		return toss;
	}
	public void setToss(String toss) {
		this.toss = toss;
	}
	public String getSpecialStatus() {
		return specialStatus;
	}
	public void setSpecialStatus(String specialStatus) {
		this.specialStatus = specialStatus;
	}
	public String getMatchState() {
		return matchState;
	}
	public void setMatchState(String matchState) {
		this.matchState = matchState;
	}
	public String getAddOnStatus() {
		return addOnStatus;
	}
	public void setAddOnStatus(String addOnStatus) {
		this.addOnStatus = addOnStatus;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
