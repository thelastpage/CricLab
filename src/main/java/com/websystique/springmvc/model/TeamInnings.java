package com.websystique.springmvc.model;

public class TeamInnings {
	String run;
	String wkts;
	String followOn;
	String declare;
	String overs;
	String description;
	
	public String getRun() {
		return run;
	}
	public void setRun(String run) {
		this.run = run;
	}
	public String getWkts() {
		return wkts;
	}
	public void setWkts(String wkts) {
		this.wkts = wkts;
	}
	public String getFollowOn() {
		return followOn;
	}
	public void setFollowOn(String followOn) {
		this.followOn = followOn;
	}
	public String getDeclare() {
		return declare;
	}
	public void setDeclare(String declare) {
		this.declare = declare;
	}
	public String getOvers() {
		return overs;
	}
	public void setOvers(String overs) {
		this.overs = overs;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
