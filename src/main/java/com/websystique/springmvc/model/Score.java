package com.websystique.springmvc.model;

import java.util.List;

public class Score {
	List<Team> team;	
	InningsDetails details;	
	
	public List<Team> getTeam() {
		return team;
	}
	public void setTeam(List<Team> team) {
		this.team = team;
	}
	public InningsDetails getDetails() {
		return details;
	}
	public void setDetails(InningsDetails details) {
		this.details = details;
	}
	
	
	
}
