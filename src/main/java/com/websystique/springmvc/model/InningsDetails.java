package com.websystique.springmvc.model;

public class InningsDetails {
	String currentRunRate;
	String RequiredRunRate;
	String numberOfOvers;
	String currentPartnership;
	
	public String getCurrentRunRate() {
		return currentRunRate;
	}
	public void setCurrentRunRate(String currentRunRate) {
		this.currentRunRate = currentRunRate;
	}
	public String getRequiredRunRate() {
		return RequiredRunRate;
	}
	public void setRequiredRunRate(String requiredRunRate) {
		RequiredRunRate = requiredRunRate;
	}
	public String getNumberOfOvers() {
		return numberOfOvers;
	}
	public void setNumberOfOvers(String numberOfOvers) {
		this.numberOfOvers = numberOfOvers;
	}
	public String getCurrentPartnership() {
		return currentPartnership;
	}
	public void setCurrentPartnership(String currentPartnership) {
		this.currentPartnership = currentPartnership;
	}
	
}
