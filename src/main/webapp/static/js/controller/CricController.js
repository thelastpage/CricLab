'use strict';

angular.module('myApp').controller('CricController', ['$scope', 'UserService', function($scope, UserService) {
    var self = this;
    self.score1={team:'',score:'',wickets:'',ovrs:''};
    self.score2={team:'',score:'',wickets:'',ovrs:''};
    self.toss='';
    self.ground='';
    self.status='';
    self.decision='';
    self.rrr='';
    self.crr='';
    self.matchDescription='';
   


    fetchAllData();
    
    function fetchAllData(){
        UserService.fetchAllData()
            .then(
            function(d) {
            	if(d.score.team!=null){
                self.score1.team = d.score.team[0].sideName;
                self.score1.score=d.score.team[0].inngs.run;
                self.score1.wickets=d.score.team[0].inngs.wkts;
                self.score1.ovrs=d.score.team[0].inngs.overs;
                
                self.score2.team = d.score.team[1].sideName;
                self.score2.score=d.score.team[1].inngs.run;
                self.score2.wickets=d.score.team[1].inngs.wkts;
                self.score2.ovrs=d.score.team[1].inngs.overs;
                
            	}
                
                self.status=d.state.status;
                self.matchDescription=d.matchDescription;
                self.crr=d.score.details.currentRunRate;
                self.rrr=d.score.details.RequiredRunRate;
                self.toss=d.state.toss;
                self.decision=d.state.decision;
            },
            function(errResponse){
                console.error('Error while fetching Users');
            }
        );
    }

}]);
