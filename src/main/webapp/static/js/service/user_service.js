'use strict';

angular.module('myApp').factory('UserService', ['$http', '$q', function($http, $q){

    var REST_SERVICE_URI = 'http://localhost:8080/Spring4MVCAngularJSExample/cric/';

    var factory = {
    		fetchAllData: fetchAllData
    };

    return factory;

    
    function fetchAllData() {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching data');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

   
}]);
