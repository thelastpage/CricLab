<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>  
    <title>Match Details</title>  
    <style>
      .username.ng-valid {
          background-color: lightgreen;
      }
      .username.ng-dirty.ng-invalid-required {
          background-color: red;
      }
      .username.ng-dirty.ng-invalid-minlength {
          background-color: yellow;
      }

      .email.ng-valid {
          background-color: lightgreen;
      }
      .email.ng-dirty.ng-invalid-required {
          background-color: red;
      }
      .email.ng-dirty.ng-invalid-email {
          background-color: yellow;
      }

    </style>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
     <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
  </head>
  <body ng-app="myApp" class="ng-cloak">
      <div class="generic-container" ng-controller="CricController as ctrl">
          
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">{{ctrl.matchDescription}}</span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      
                      
                      <tbody>
                      <tr>
                      	<td><span style="font: bold; font-size: 22px">Toss: {{ctrl.toss}}</span></td>
                      </tr>
                      <tr>
                      	 <td><span style="font: bold; font-size: 22px">Decision: {{ctrl.decision}}</span></td>
                      </tr>
                      </tbody>
                      
                      <tbody>
                          <tr>
                              <td><span ng-bind="ctrl.score1.team"></span></td>
                              <td><span ng-bind="ctrl.score1.score"></span></td>
                              <td><span ng-bind="ctrl.score1.wickets"></span></td>
                              <td><span ng-bind="ctrl.score1.ovrs"></span></td> 
                              
                          </tr>
                          
                          <tr>
                              <td><span ng-bind="ctrl.score2.team"></span></td>
                              <td><span ng-bind="ctrl.score2.score"></span></td>
                              <td><span ng-bind="ctrl.score2.wickets"></span></td>
                              <td><span ng-bind="ctrl.score2.ovrs"></span></td> 
                          </tr>
                          </tbody>
                          <tbody>
                          <tr>
                           <td><span ng-bind="ctrl.status"></span></td>
                          </tr>
                          </tbody>
                          
                          <tr>
                          <td>
                              <button type="button" ng-click="ctrl.fetchAllData()" class="btn btn-success custom-width">Refresh</button>
                          </td>
                          </tr>
                  </table>
              </div>
          </div>
      </div>
      
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
      <script src="<c:url value='/static/js/app.js' />"></script>
      <script src="<c:url value='/static/js/service/user_service.js' />"></script>
      <script src="<c:url value='/static/js/controller/CricController.js' />"></script>
  </body>
</html>